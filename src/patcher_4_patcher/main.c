/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <mkrutik@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/16 20:47:50 by mkrutik           #+#    #+#             */
/*   Updated: 2019/06/20 17:07:45 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "famine.h"

void		*get_section_by_name64(void *file, const char *name, char res)
{
	size_t		i;
	Elf64_Shdr	*shstrtable_hdr;
	char		*shstrtable;

	shstrtable_hdr = SHDRS64(file) + ((Elf64_Ehdr*)file)->e_shstrndx;
	shstrtable = (char*)(file + shstrtable_hdr->sh_offset);
	i = 0;
	while (i < ((Elf64_Ehdr*)file)->e_shnum)
	{
		if (ft_strcmp(shstrtable + SHDRS64(file)[i].sh_name, name) == 0)
		{
			if (res == HDR)
				return (SHDRS64(file) + i);
			else
				return (file + SHDRS64(file)[i].sh_offset);
		}
		i++;
	}
	return (NULL);
}


Elf64_Sym	*find_symbol_by_name64(void *file, const char *name)
{
	Elf64_Shdr	*symtab_hdr;
	Elf64_Sym	*symtab;
	char		*strtab;
	size_t		i;

	symtab_hdr = ((Elf64_Shdr*)get_section_by_name64(file, ".symtab", HDR));
	symtab = (Elf64_Sym*)(file + symtab_hdr->sh_offset);
	strtab = get_section_by_name64(file, ".strtab", SECT);
	i = 0;
	while (((i + 1)*sizeof(*symtab)) <= symtab_hdr->sh_size)
	{
		if (ft_strcmp(strtab + symtab[i].st_name, name) == 0)
			return (symtab + i);
		i++;
	}
	return (NULL);
}


t_status	update_text_size(void *file)
{
	Elf64_Sym   *op_sym;
	Elf64_Shdr  *text;
	size_t      offset;
	uint64_t    *arg_ptr;

	if ((op_sym = find_symbol_by_name64(file, "text_size")) == NULL ||
		(text = get_section_by_name64(file, ".text", HDR)) == NULL ||
		op_sym->st_value < text->sh_addr)
		return (FAIL);
	offset = text->sh_offset + (op_sym->st_value - text->sh_addr);
	arg_ptr = (uint64_t*)(file + offset);
	*arg_ptr = text->sh_size;
	return (SUCCESS);
}


int			main(int argc, char *argv[])
{
    const char  error_arg[] = "Error: wrong number of arguments !\n";
    const char  error_parse[] = "File check failed!\n";
    const char  error_patching[] = "Patching failed!\n";
    t_file      bin;

    if (argc == 2)
    {
        bin.file_name = argv[1];
        if (SUCCESS == get_file_content(&bin))
        {
            if (SUCCESS != elf_64_parse(&bin))
                _ft_write(STDOUT_FILENO, error_parse, ft_strlen(error_parse));
            else if (FAIL == update_text_size(bin.data))
                _ft_write(STDOUT_FILENO, error_patching, ft_strlen(error_patching));
            _ft_munmap(bin.data, bin.mem_size);
        }
    }
    else
        _ft_write(STDOUT_FILENO, error_arg, ft_strlen(error_arg));
    return (EXIT_SUCCESS);
}