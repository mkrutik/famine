section	.text
	global	ft_stat, ft_truncate, ft_getdents64

ft_stat:	;int ft_fstatat(char *name, struct stat *buff, int flag);
	mov		rax, 4
	syscall
	ret

ft_truncate: ;int ft_truncate(const char *path, off_t length);
	mov		rax, 76
	syscall
	ret

ft_getdents64: ;int ft_getdents64(unsigned int fd, struct linux_dirent64 *dirp, unsigned int count);
	mov		rax, 217
	syscall
	ret
