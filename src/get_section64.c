/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_section64.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adzikovs <adzikovs@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/22 20:42:01 by mkrutik           #+#    #+#             */
/*   Updated: 2019/06/13 10:41:26 by adzikovs         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "famine.h"

void	*get_section_by_index64(void *file, size_t index, char res)
{
	if (index >= ((Elf64_Ehdr*)file)->e_shnum)
		return (NULL);
	if (res == HDR)
		return (SHDRS64(file) + index);
	else
		return (file + SHDRS64(file)[index].sh_offset);
}

void	*get_section_by_name64(void *file, const char *name, char res)
{
	size_t		i;
	Elf64_Shdr	*shstrtable_hdr;
	char		*shstrtable;

	shstrtable_hdr = SHDRS64(file) + ((Elf64_Ehdr*)file)->e_shstrndx;
	shstrtable = (char*)(file + shstrtable_hdr->sh_offset);
	i = 0;
	while (i < ((Elf64_Ehdr*)file)->e_shnum)
	{
		if (ft_strcmp(shstrtable + SHDRS64(file)[i].sh_name, name) == 0)
		{
			if (res == HDR)
				return (SHDRS64(file) + i);
			else
				return (file + SHDRS64(file)[i].sh_offset);
		}
		i++;
	}
	return (NULL);
}
