/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   defines.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <mkrutik@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/26 17:19:11 by mkrutik           #+#    #+#             */
/*   Updated: 2019/06/20 18:43:59 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef DEFINES_H
# define DEFINES_H
# include <errno.h>

# define LOG(str) _ft_write(1, str, sizeof(str) - 1);
# undef LOG
# define LOG(str) ;
# define MMAP_FAILED(ret) ((ret) == (void*)-EACCES || (ret) == (void*)-EAGAIN ||\
							(ret) == (void*)-EBADF || (ret) == (void*)-EEXIST ||\
							(ret) == (void*)-EINVAL || (ret) == (void*)-ENFILE ||\
							(ret) == (void*)-ENODEV || (ret) == (void*)-ENOMEM ||\
							(ret) == (void*)-EOVERFLOW || (ret) == (void*)-EPERM ||\
							(ret) == (void*)-ETXTBSY || (ret) == MAP_FAILED)

# define VERSION_PATCH 0x12345678
# define PAGE_SIZE 8192

# define BUFF_SIZE 1024

# define HDR 0
# define SECT 1

# define ENTRY(p) (((Elf64_Ehdr*)p)->e_entry)
# define PHNUM64(p) (((Elf64_Ehdr*)p)->e_phnum)
# define PHDRS64(p) ((Elf64_Phdr*)((void*)p + ((Elf64_Ehdr*)p)->e_phoff))
# define SHNUM64(p) (((Elf64_Ehdr*)p)->e_shnum)
# define SHDRS64(p) ((Elf64_Shdr*)((void*)p + ((Elf64_Ehdr*)p)->e_shoff))

#endif
